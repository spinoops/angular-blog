import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  postList = [
      {
          title: 'Mon premier post',
          content: 'Postremo ad id indignitatis est ventum, ut cum peregrini ob formidatam haut ita dudum alimentorum inopiam pellerentur.',
          loveIts: 0,
          created_at: new Date('2018-12-24')
      },
      {
          title: 'Mon deuxième post',
          content: 'Postremo ad id indignitatis est ventum, ut cum peregrini ob formidatam haut ita dudum alimentorum inopiam pellerentur.',
          loveIts: 0,
          created_at: new Date('2018-12-25')
      },
      {
          title: 'Encore un post',
          content: 'Postremo ad id indignitatis est ventum, ut cum peregrini ob formidatam haut ita dudum alimentorum inopiam pellerentur.',
          loveIts: 0,
          created_at: new Date('2018-12-26')
      }
  ];

}
